<?php

abstract class Creator
{
    /**
     * @var Registry
     */
    protected $registry;

    function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    public function getProduct()
    {
        $product = $this->createProduct();
        $this->registry->addProduct($product);

        return $product;
    }

    /**
     * @return Product
     */
    abstract function createProduct();
}
