<?php

class Registry
{
    /**
     * @var Product[]
     */
    public $products = [];

    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }
} 
